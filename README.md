# Dynatrace

This project aims to improve the way we run the Easy Travel demo app in Docker for easier setup and usage with Dynatrace AppMon.
The Easy Travel app is very useful for generating sample traffic data and interpreting that data in Dynatrace.